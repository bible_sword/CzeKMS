#!/usr/bin/python3
import logging
import sys
import xml.sax
from xml.sax.saxutils import XMLFilterBase, XMLGenerator
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)


class MilestoneFilter(XMLFilterBase):
    def __init__(self, upstream, downstream):
        XMLFilterBase.__init__(self, upstream)
        self._downstream = downstream
        self._verse_once_done = False
        self._in_quote = False

    def startDocument(self):
        pass

    def endDocument(self):
        pass

    def startElement(self, name, attrs):
        if name == "verse":
            logging.debug("start of verse, started_verse = %s, in_quote = %s",
                          self._verse_once_done, self._in_quote)
            self._verse_once_done = True
            if 'eID' in attrs:
                if self._in_quote:
                    logging.debug("surrounding end of verse with q construct.")
                    self._downstream.endElement("q")
                    self._downstream.endElement(name)
                else:
                    logging.debug("substitute ending verse")
                    self._downstream.endElement(name)
                return
        if name == "q":
            logging.debug("getting into q")
            self._in_quote = True
        logging.debug("starting element %s", name)
        self._downstream.startElement(name, attrs)
        if (name == "verse") and self._in_quote:
                self._downstream.startElement("q", {"marker": "'"})

    def endElement(self, name):
        if name == "verse":
            logging.debug("end of verse, done_verse = %s, in_quote = %s",
                          self._verse_once_done, self._in_quote)
            if self._verse_once_done:
                logging.debug("skipping duplicate end of verse")
                self._verse_once_done = False
            else:
                logging.debug("regular ending verse")
                if self._in_quote:
                    logging.debug("surrounding end of verse with q construct.")
                    self._downstream.endElement("q")
                    self._downstream.endElement(name)
                    self._downstream.startElement("q", {"marker": "'"})
                else:
                    logging.debug("ending element %s", name)
                    self._downstream.endElement(name)
        elif name == "q":
            logging.debug("ending q")
            self._downstream.endElement("q")
            self._in_quote = False
        else:
            logging.debug("ending element %s", name)
            self._downstream.endElement(name)

    def characters(self, content):
        self._downstream.characters(content)


if __name__ == "__main__":
    downstream_handler = XMLGenerator(encoding="utf-8",
                                      short_empty_elements=True)
    MilestoneFilter(xml.sax.make_parser(),
                    downstream_handler).parse(sys.argv[1])
